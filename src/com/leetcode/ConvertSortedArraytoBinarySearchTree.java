package com.leetcode;

public class ConvertSortedArraytoBinarySearchTree {

	public static void main(String[] args) {
		ConvertSortedArraytoBinarySearchTree c = new ConvertSortedArraytoBinarySearchTree();
		int[] num = { 1, 2, 3, 4, 5, 6, 7, 8 };
		TreeNode root = c.sortedArrayToBST(num);
		System.out.println(root);
	}

	public TreeNode sortedArrayToBST(int[] num) {
		return insert(num,0,num.length-1);
	}

	public TreeNode insert(int[] num , int begin ,int end) {
		if(begin>end){
			return null;
		}
		int mid = (begin+end)/2;
		TreeNode node = new TreeNode(num[mid]);
		node.left = insert(num,begin,mid-1);
		node.right = insert(num,mid+1,end);
		return node;
	}

}
