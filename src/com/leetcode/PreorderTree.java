package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class PreorderTree {
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		TreeNode left2 = new TreeNode(2);
		root.left = left2;
		PreorderTree bean = new PreorderTree();
		List<Integer> list = bean.preorderTraversal(root);
		System.out.println(list);
	}

	public ArrayList<Integer> preorderTraversal(TreeNode root) {
		ArrayList<Integer> list = new ArrayList<>();
		trave(list,root);
		return list;
	}
	
	public void trave(ArrayList<Integer> list,TreeNode node){
		if(node!=null){
			list.add(node.val);
			trave(list,node.left);
			trave(list,node.right);
		}
	}
}
