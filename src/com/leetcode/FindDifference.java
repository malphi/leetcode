package com.leetcode;

import java.util.HashSet;

/**
 * Created by rfma on 17/1/18.
 */
public class FindDifference {
	public static void main(String[] args) {
		FindDifference df = new FindDifference();
		String s = "abcd";
		String t = "abcde";
		System.out.println(df.findTheDifference(s, t));
	}

	public char findTheDifference(String s, String t) {
		int sum = 0;
		for (int i = 0; i < t.length(); i++) {
			char c = t.charAt(i);
			sum += c;
		}
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			sum -=c;
		}
		return (char)sum;
	}
	public char findTheDifference2(String s, String t) {
		int n = t.length();
		char c = t.charAt(n - 1);
		for (int i = 0; i < n - 1; ++i) {
			c ^= s.charAt(i);
			c ^= t.charAt(i);
		}
		return c;
	}
}
