package com.leetcode;


public class RemoveElement {
	public static void main(String[] args) {
		int[] A = { 4, 5 };
		int x = removeElement(A, 4);
//		System.out.println(x);
	}

	public static int removeElement(int[] A, int elem) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < A.length; i++) {
			int x = A[i];
			if (x != elem) {
				count++;
			}else{
				A[i] = A[A.length-1];
				A[A.length-1] = x;
			}
		}
		return count;
	}

	public static int test(int[] A, int elem) {
		int start = 0;
		for (int i = 0; i < A.length; i++){
			if (elem != A[i]) {
				A[start++] = A[i];
			}
		}
		for (int i = 0; i < A.length; i++) {
			System.out.println(A[i]);
		}
		return start;
	}
}
