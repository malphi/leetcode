package com.leetcode;

public class RotateImage {
	public static void main(String[] args) {
		int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};
		RotateImage.rotate(matrix);
	}

	public static void rotate(int[][] matrix) {
		print(matrix);
		int i, j, temp;
		int n = matrix.length;
		for (i = 0; i < n / 2; ++i) {
			for (j = i; j < n - 1 - i; ++j) {
				temp = matrix[j][n - i - 1];
				matrix[j][n - i - 1] = matrix[i][j];
				matrix[i][j] = matrix[n - j - 1][i];
				matrix[n - j - 1][i] = matrix[n - i - 1][n - j - 1];
				matrix[n - i - 1][n - j - 1] = temp;
			}
		}
		print(matrix);
	}

	private static void print(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}
		
	}
}
