package com.leetcode;

public class SwapNodePair {
	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);
		System.out.println(head);
		head = swapPairs(head);
		System.out.println(head);
	}

	public static ListNode swapPairs(ListNode head) {
		if(head==null){
			return head;
		}
		ListNode node = head;
		int i = 1;
		while(node.next!=null){
			i++;
			if(i%2==0){
				ListNode tmp = node.next; 
				node.next = node.next.next;
				tmp.next = node;
			}
		}
		return head;
	}
}
