package com.leetcode;

public class Stock {
	public static void main(String[] args) {
		Stock s = new Stock();
		int[] prices = { 1, 2, 3, 4, 6 };
		int x = s.maxProfit(prices);
		System.out.println(x);
	}

	public int maxProfit1(int[] prices) {
		int max = 0;
		for (int i = 0; i < prices.length - 1; i++) {
			for (int j = i + 1; j < prices.length; j++) {
				int tmp = prices[j] - prices[i];
				if (tmp > max) {
					max = tmp;
				}
			}
		}
		return max;
	}

	public int maxProfit(int[] prices) {
		int max = 0;
		for (int i = 1; i < prices.length; i++) {
			int tmp = prices[i] - prices[i-1];
			if (tmp > 0) {
				max += tmp;
			}
		}
		return max;
	}
}
