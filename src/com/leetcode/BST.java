package com.leetcode;

public class BST {
	public static void main(String[] args) {
		BST b = new BST();
		System.out.println(b.numTrees(3));
	}

	public int numTrees(int n) {
		return numTrees(1, n);

	}

	int numTrees(int start, int end) {
		if (start >= end)
			return 1;

		int totalNum = 0;
		for (int i = start; i <= end; ++i)
			totalNum += numTrees(start, i - 1) * numTrees(i + 1, end);
		return totalNum;
	}
}
