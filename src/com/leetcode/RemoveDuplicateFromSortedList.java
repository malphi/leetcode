package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class RemoveDuplicateFromSortedList {
	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		ListNode node = new ListNode(1);
		head.next = node;
//		ListNode node3 = new ListNode(1);
//		node.next = node3;
		deleteDuplicates2(head);
	}

	public static ListNode deleteDuplicates2(ListNode head) {
		List<Integer> list = new ArrayList<>();
		ListNode node = head;
		ListNode last = head;
		while(node!=null){
			if(list.contains(node.val)){
				last.next = node.next;
			}else{
				list.add(node.val);
				last = node;
			}
			node = node.next;
		}
		return head;
	}

}
