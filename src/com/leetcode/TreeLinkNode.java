package com.leetcode;

public class TreeLinkNode {
	int val;
	TreeLinkNode left, right, next;

	TreeLinkNode(int x) {
		val = x;
	}

	@Override
	public String toString() {
		return "TreeLinkNode [val=" + val + ", next=" + next + "]";
	}

}