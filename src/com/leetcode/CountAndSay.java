package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class CountAndSay {

	public static void main(String[] args) {
		CountAndSay cas = new CountAndSay();
		System.out.println(cas.countAndSayIteration(5));
	}

	static String countAndSayRecursive(int n) {
		if (n < 1)
			return null;
		if (n == 1)
			return "1";

		String str = countAndSayRecursive(n - 1);
		StringBuilder sb = new StringBuilder();
		int count = 1;
		// we start from the second character, not the first one.
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == str.charAt(i - 1)) {
				count++;
			} else {
				sb.append(count);
				sb.append(str.charAt(i - 1));
				count = 1;
			}
		}
		// handle the last character case
		sb.append(count);
		sb.append(str.charAt(str.length() - 1));
		return sb.toString();
	}

	static String countAndSayIteration(int n) {
		if (n < 1)
			return null;

		String str = "1";
		StringBuilder sb = new StringBuilder();
		for (int j = 2; j <= n; j++) {
			int count = 1;
			for (int i = 1; i < str.length(); i++) {
				if (str.charAt(i) == str.charAt(i - 1)) {
					count++;
				} else {
					sb.append(count);
					sb.append(str.charAt(i - 1));
					count = 1;
				}
			}
			sb.append(count);
			sb.append(str.charAt(str.length() - 1));
			str = sb.toString();
			sb.setLength(0);
		}
		return str;
	}
}
