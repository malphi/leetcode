package com.leetcode;

public class Strstr {

	public String strStr(String haystack, String needle) {
		if (haystack == null || needle == null)
			return null;
		if (needle.length() == 0)
			return haystack;
		if (needle.length() > haystack.length())
			return null;
		for (int i = 0; i <= haystack.length() - needle.length(); i++) {
			boolean isPass = true;
			for (int j = 0; j < needle.length(); j++) {
				if (haystack.charAt(i + j) != needle.charAt(j)) {
					isPass = false;
					break;
				}
			}
			if (isPass == true) {
				return haystack.substring(i);
			}
		}
		return null;
	}

	public static void main(String[] args) {
		Strstr bean = new Strstr();
		String x = bean.strStr("aaa", "a");
		System.out.println(x);
		x.indexOf("");
	}
}
