package com.leetcode;

import java.util.Stack;

public class MaxDepthTree {
	public int maxDepth(TN root) {
		int x = 0;
		if (root != null) {
			int ld = maxDepth(root.left);
			int rd = maxDepth(root.right);
			x = ld > rd ? ld + 1 : rd + 1;
		}
		return x;
	}

	public int TreeDeep(TN root) {
		int d = 0;
		Stack<TN> S = new Stack<>();
		Stack<Integer> tag = new Stack<>();
		TN p = root;
		while (p != null || !S.isEmpty()) {
			while (p != null) {
				S.push(p);
				tag.push(0);
				p = p.left;
			}
			if (tag.pop() == 1) {
				d = d > S.size() ? d : S.size();
				S.pop();
				tag.pop();
				p = null;
			} else {
				p = S.pop();
				p = p.right;
				tag.pop();
				tag.push(1);
			}
		}
		return d;
	}

	public static void main(String[] args) {
		TN root = new TN(1);
		root.left = new TN(2);
		root.right = new TN(3);
		root.right.right = new TN(4);
		MaxDepthTree t = new MaxDepthTree();
//		int x = t.maxDepth(root);
		int x = t.TreeDeep(root);
		System.out.println(x);
	}
}

class TN {
	int val;
	TN left;
	TN right;

	TN(int x) {
		val = x;
	}
}