package com.leetcode;

import com.sun.tools.javac.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rfma on 17/2/6.
 */
public class RansomNote {
	public static void main(String[] args) {
		System.out.println(canConstruct("aa","ab"));
	}
	public static boolean canConstruct(String ransomNote, String magazine) {
		if (magazine.length() < ransomNote.length()) {
			return false;
		}
		Map<Character, Integer> map = new HashMap<>();
		for (int i = 0; i < magazine.length(); i++) {
			char c = magazine.charAt(i);
			Integer count = map.get(c);
			if (count == null) {
				count = 1;
			} else {
				count++;
			}
			map.put(c,count);
		}
		System.out.println(map);
		for (int i = 0; i < ransomNote.length(); i++) {
			char c = ransomNote.charAt(i);
			Integer count = map.get(c);
			System.out.println("c="+c+" ,count="+count);
			if (count == null || count==0) {
				return false;
			}else{
				map.put(c, count - 1);
			}
			System.out.println("map= "+map);
		}
		return true;
	}
}
