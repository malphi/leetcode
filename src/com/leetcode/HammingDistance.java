package com.leetcode;

/**
 * 汉明距离:同样长度的字符串变成相等的需要替换N个字符的长度
 * The Hamming distance between two integers is the number of positions at which the corresponding bits are different.
 * 0 ≤ x, y < 2^31.
 *
 * @author mrf
 */
public class HammingDistance {

	public static void main(String[] args) {
		int x = 2;
		int y = 4;
		int distance = HammingDistance.hammingDistance(x, y);
		System.out.println(distance);
	}

	/*
		数为num, num & (num - 1)可以快速地移除最右边的bit 1， 一直循环到num为0, 总的循环数就是num中bit 1的个数
		Input: x = 1, y = 4
		Output: 2
		Explanation:
		1   (0 0 0 1)
		4   (0 1 0 0)
	 */
	public static int hammingDistance(int x, int y) {
		if (x == y) {
			return 0;
		}
		int dis = 0;
		int val = x ^ y;
		System.out.println("val="+val);
		while(val!=0){
			dis++;
			val &=val-1;
		}
		System.out.println("dis=" + dis);
		return dis;
	}
	public int hammingDistance1(int x, int y) {
		return Integer.bitCount(x ^ y);
	}
	/*
		两个数字之间的汉明距离就是其二进制数对应位不同的个数，
		最直接的做法就是按位分别取出两个数对应位上的数并异或,把为1的情况累加起来就是汉明距离了
	 */
	public int hammingDistance2(int x, int y) {
		int xor = x ^ y, count = 0;
		for (int i=0;i<32;i++) count += (xor >> i) & 1;
		return count;
	}
}
