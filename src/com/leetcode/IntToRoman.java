package com.leetcode;

public class IntToRoman {
	private static final int[] numbers = { 1000, 900, 500, 400, 100, 90, 50,
			40, 10, 9, 5, 4, 1 };
	private static final String[] values = { "M", "CM", "D", "CD", "C", "XC",
			"L", "XL", "X", "IX", "V", "IV", "I" };

	public static String intToRoman(int num) {
		String res = "";
		int i = 0;
		while (num > 0) {
			if (num >= numbers[i]) {
				res += values[i];
				num -= numbers[i];
			}else{
				i++;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		String roman = intToRoman(3999);
		System.out.println(roman);
	}
}
