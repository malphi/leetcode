package com.leetcode;

import java.util.ArrayList;

public class GrayCode {
	
	public static void main(String[] args) {
		
		System.out.println(grayCode(2));
	}

	public static ArrayList<Integer> grayCode(int n) {
		ArrayList<Integer> res = new ArrayList<>();
		int num = 1<<n;
        int i = 0;
		while(i<num)
			res.add((i>>1)^(i++));
		return res;

	}
}
