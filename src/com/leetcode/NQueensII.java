package com.leetcode;

public class NQueensII {

	public static void main(String[] args) {
		NQueensII nq = new NQueensII();
		int x = nq.totalNQueens(4);
		System.out.println(x);
	}

	int result = 0;

	public int totalNQueens(int n) {
		result = 0;
		solve(new int[n], 0);
		return result;
	}

	public void solve(int[] arr, int pos) {
		int size = arr.length;
		if (pos == size) {
			result++;
			return;
		}
		for (int i = 0; i < size; i++)
			if (isValid(arr, pos, i)) {
				arr[pos] = i;
				solve(arr, pos + 1);
			}
	}

	public boolean isValid(int[] arr, int pos, int check) {
		for (int i = 0; i < pos; i++)
			if (check == arr[i]
					|| Math.abs(pos - i) == Math.abs(check - arr[i]))
				return false;
		return true;
	}

}
