package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rfma on 17/1/10.
 */
public class FindDisappearedNumbers {
	public static void main(String[] args) {
		FindDisappearedNumbers bean = new FindDisappearedNumbers();
		int[] nums ={4,3,2,7,8,2,3,1};
		bean.findDisappearedNumbers2(nums);
	}
	public List<Integer> findDisappearedNumbers(int[] nums) {
		int[] bucket = new int[nums.length+1];
		for (int i = 0; i < nums.length; i++) {
			int element = nums[i];
			bucket[element] = 1;
		}
		List<Integer> ret = new ArrayList<>();
		for (int i = 1; i < bucket.length; i++) {
			if(bucket[i]==0){
				ret.add(i);
			}
		}
		return ret;
	}

	/**
	 * The basic idea is that we iterate through the input array and mark elements as
	 * negative using nums[nums[i] -1] = -nums[nums[i]-1].
	 * In this way all the numbers that we have seen will be marked as negative.
	 * In the second iteration, if a value is not marked as negative, it implies we have never seen that index before, so just add it to the return list.
	 * @param nums
	 * @return
	 */
	public List<Integer> findDisappearedNumbers2(int[] nums) {
		List<Integer> ret = new ArrayList<Integer>();

		for(int i = 0; i < nums.length; i++) {
			int val = Math.abs(nums[i]) - 1;
			System.out.println(i+" "+nums[i]+" "+val);
			if(nums[val] > 0) {
				nums[val] = -nums[val];
			}
		}

		for(int i = 0; i < nums.length; i++) {
			if(nums[i] > 0) {
				ret.add(i+1);
			}
		}
		return ret;
	}
	public List<Integer> findDisappearedNumbers3(int[] nums) {
		List<Integer> res = new ArrayList<>();
		int n = nums.length;
		for (int i = 0; i < nums.length; i ++) nums[(nums[i]-1) % n] += n;
		System.out.println(Arrays.toString(nums));
		for (int i = 0; i < nums.length; i ++) if (nums[i] <= n) res.add(i+1);
		System.out.println(res);
		return res;
	}
}
