package com.leetcode;

import java.util.Arrays;

public class SpiralMatrixII {

	public static void main(String[] args) {
		SpiralMatrixII sm = new SpiralMatrixII();
		int[][] arr = sm.generateMatrix(4);
		System.out.println("arr is ");
		// show(arr);
	}

	private static void show(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.println(Arrays.toString(arr[i]));
		}
		System.out.println();
	}

	public int[][] generateMatrix(int n) {
		int[][] matrix = new int[n][n];
		int start = 0, end = n - 1;
		int num = 1;
		while (start < end) {
			// 第一行填充
			for (int j = start; j < end; j++) {
				matrix[start][j] = num;
				num++;
			}
			// 最后一列填充
			for (int i = start; i < end; i++) {
				matrix[i][end] = num;
				num++;
			}
			// 最后一行
			for (int j = end; j > start; j--) {
				matrix[end][j] = num;
				num++;
			}
			// 第一列
			for (int i = end; i > start; i--) {
				matrix[i][start] = num;
				num++;
			}
			start++;
			end--;
		}
		if (start == end) {
			matrix[start][start] = num;
		}
		return matrix;
	}
}
