package com.leetcode;

/**
 * Created by rfma on 17/1/17.
 * Calculate the sum of two integers a and b, but you are not allowed to use the operator + and -
 */
public class SumOfTwoIntegers {
	public static void main(String[] args) {

	}

	public int getSum(int a, int b) {
		if (a == 0) return b;
		if (b == 0) return a;

		while (b != 0) {
			int carry = a & b;
			a = a ^ b;
			b = carry << 1;
		}

		return a;
	}

	// Recursive
	public int getSum2(int a, int b) {
		return (b == 0) ? a : getSum(a ^ b, (a & b) << 1);
	}
}
