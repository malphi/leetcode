package com.leetcode;

/**
 * Created by rfma on 17/1/20.
 */
public class MaxConsecutiveOnes {
	public static void main(String[] args) {
		MaxConsecutiveOnes m = new MaxConsecutiveOnes();
		int[] nums = {1,1,0,1,1,1};
		m.findMaxConsecutiveOnes(nums);
	}
	/*
		Given a binary array, find the maximum number of consecutive 1s in this array.
		Input: [1,1,0,1,1,1]
		Output: 3
		Explanation: The first two digits or the last three digits are consecutive 1s.
	    The maximum number of consecutive 1s is 3.
	 */
	public int findMaxConsecutiveOnes(int[] nums) {
		int ret = 0;
		int max = 0;
		for (int i = 0; i < nums.length; i++) {
			if(nums[i]==1){
				ret++;
				if(ret>max){
					max = ret;
				}
			}else{
				ret = 0;
			}

		}
		return max;
	}
	public int findMaxConsecutiveOnes2(int[] nums) {
		int maxHere = 0, max = 0;
		for (int n : nums)
			max = Math.max(max, maxHere = n == 0 ? 0 : maxHere + 1);
		return max;
	}
}
