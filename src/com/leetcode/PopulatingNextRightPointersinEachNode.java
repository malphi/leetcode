package com.leetcode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PopulatingNextRightPointersinEachNode {

	public static void main(String[] args) {
		TreeLinkNode root = new TreeLinkNode(1);
		root.left = new TreeLinkNode(2);
		root.right = new TreeLinkNode(3);
		root.left.left = new TreeLinkNode(4);
		root.left.right = new TreeLinkNode(5);
		root.right.left = new TreeLinkNode(6);
		root.right.right = new TreeLinkNode(7);
		PopulatingNextRightPointersinEachNode p = new PopulatingNextRightPointersinEachNode();
		p.connect2(root);
	}

	public void connect(TreeLinkNode root) {
		Map<Integer, List<TreeLinkNode>> map = new HashMap<>();
		check(root, 1, map);
		System.out.println(map);
	}

	public void check(TreeLinkNode node, int level,
			Map<Integer, List<TreeLinkNode>> map) {
		if (node == null) {
			return;
		}
		List<TreeLinkNode> list = map.get(level);
		if (list == null) {
			list = new LinkedList<TreeLinkNode>();
			map.put(level, list);
		}
		if (list.size() > 0) {
			TreeLinkNode last = list.get(list.size() - 1);
			last.next = node;
		}
		list.add(node);
		check(node.left, level + 1, map);
		check(node.right, level + 1, map);
	}

	public void connect2(TreeLinkNode root) {
		if (root == null)
			return;
		TreeLinkNode left = root.left;
		TreeLinkNode right = root.right;
		if (left != null) {
			left.next = right;
		}
		if (right != null && root.next != null) {
			right.next = root.next.left;
		}
		connect2(left);
		connect2(right);
	}
}
