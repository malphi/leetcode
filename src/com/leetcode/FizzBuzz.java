package com.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by rfma on 17/1/5.
 * Spec: Write a program that outputs the string representation of numbers from 1 to n.
 * 3的倍数用Fizz代替，5的倍数用Buzz代替，3和5的公倍数用FizzBuzz代替
 */
public class FizzBuzz {
	public static void main(String[] args) {
		FizzBuzz fb = new FizzBuzz();
		System.out.println(fb.fizzBuzz(15));
	}

	public List<String> fizzBuzz(int n) {
	    if (n <= 0) {
			return Collections.emptyList();
		}
		List<String> res = new ArrayList<>();
		for (int i = 1; i <= n; i++) {
			if (i % 3 == 0 && i % 5 == 0) {
				res.add("FizzBuzz");
			} else if (i % 3 == 0) {
				res.add("Fizz");
			} else if (i % 5 == 0) {
				res.add("Buzz");
			} else {
				res.add(String.valueOf(i));
			}
		}
		return res;
	}
	//不使用%的解决方案
	public List<String> fizzBuzz1(int n) {
		List<String> ret = new ArrayList<>(n);
		for (int i = 1, fizz = 0, buzz = 0; i <= n; i++) {
			fizz++;
			buzz++;
			if (fizz == 3 && buzz == 5) {
				ret.add("FizzBuzz");
				fizz = 0;
				buzz = 0;
			} else if (fizz == 3) {
				ret.add("Fizz");
				fizz = 0;
			} else if (buzz == 5) {
				ret.add("Buzz");
				buzz = 0;
			} else {
				ret.add(String.valueOf(i));
			}
		}
		return ret;
	}
}
