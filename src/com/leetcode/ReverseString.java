package com.leetcode;

import com.sun.tools.javac.util.StringUtils;

/**
 * Created by rfma on 17/1/6.
 */
public class ReverseString {
	public static void main(String[] args) {
		ReverseString rs = new ReverseString();
		System.out.println(rs.reverseString("hello"));
	}
	//逆序循环
	public String reverseString(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		StringBuilder ret = new StringBuilder("");
		for (int i = s.length()-1; i >=0 ; i--) {
			ret.append(s.charAt(i));
		}
		return ret.toString();
	}
	//自带函数
	public String reverseString2(String s) {
		return new StringBuilder(s).reverse().toString();
	}
	//效率最高，只遍历一半
	public String reverseString3(String s) {
		char[] word = s.toCharArray();
		int i = 0;
		int j = s.length() - 1;
		while (i < j) {
			char temp = word[i];
			word[i] = word[j];
			word[j] = temp;
			i++;
			j--;
		}
		return new String(word);
	}
}
