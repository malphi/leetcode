package com.leetcode;


public class MergeTwoSortedList {
	
	public static void main(String[] args) {
		//l1
		ListNode l1 = new ListNode(2);
		//l2
		ListNode l2 = new ListNode(1);
		//
		MergeTwoSortedList m = new MergeTwoSortedList();
		ListNode res = m.mergeTwoLists(l1, l2);
		System.out.println(res);
	}

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		if(l1==null){
			return l2;
		}
		if(l2==null){
			return l1;
		}
		ListNode root = null;
		if(l1.val<=l2.val){
			root = l1;
			root.next = mergeTwoLists(l1.next,l2);
		}else{
			root = l2;
			root.next = mergeTwoLists(l1,l2.next);
		}
		return root;
	}


}
