package com.leetcode;

public class MiniWindows {

	public String minWindow(String S, String T) {
		if (S == null || T == null) {
			return "";
		}
		if (S.equals(T)) {
			return S;
		}
		String result = null;
		for (int x = 0; x < S.length(); x++) {
			int start = x;
			int end = 0;
			int count = 0;
			for (int i = x; i < S.length(); i++) {
				char s = S.charAt(i);
				for (int j = 0; j < T.length(); j++) {
					char t = T.charAt(j);
					if (s == t) {
						count++;
						if (count == T.length()) {
							end = i;
						}
					}
				}
				if (count == T.length()) {
					String tmp = S.substring(start, end+1);
					if (result == null) {
						result = tmp;
					} else {
						if (tmp.length() < result.length()) {
							result = tmp;
						}
					}
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		MiniWindows c = new MiniWindows();
		System.out.println("=" + c.minWindow("obzcopzocynyrsgsarijyxnkpnukkrvzccaoguieu", "abc"));
	}
}
