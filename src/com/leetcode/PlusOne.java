package com.leetcode;

import java.util.Arrays;

/**
 * @author mrf 给你一个用数组表示的数，求加一之后的结果，结果还是用数组表示。
 *
 */
public class PlusOne {
	public static void main(String[] args) {
		int[] x = { 9,9,9 };
		PlusOne po = new PlusOne();
		System.out.println(Arrays.toString(po.plusOne(x)));
	}

	public int[] plusOne(int[] digits) {
		if (digits == null || digits.length == 0) {
			int[] x = { 1 };
			return x;
		}
		int i ;
		for (i = digits.length - 1; i >= 0; i--) {
			if(digits[i]!=9){
				++digits[i];
				return digits;
			}else{
				digits[i] = 0;
			}
		}
		if(i<0){
			int[] result = new int[digits.length+1];
			result[0] = 1;
			return result;
		}
		return digits;
	}
}
