package com.leetcode;

public class SearchInsertPositon {

	public static void main(String[] args) {
		int[] A = { 1, 3, 5, 6 };
		int x = searchInsert(A, 0);
		System.out.println(x);
	}

	public static int searchInsert(int[] A, int target) {
		int pos = 0;
		if (A == null || A.length == 0) {
			return 0;
		}
		for (int i = 0; i < A.length; i++) {
			int x = A[i];
			if(x==target){
				return i;
			}
			if(x<target){
				pos++;
			}
		}
		return pos;
	}
}
