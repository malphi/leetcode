package com.leetcode;


public class SymmetricTree {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(2);
		root.left.right = new TreeNode(3);
		root.right.right = new TreeNode(3);
		SymmetricTree st = new SymmetricTree();
		boolean res = st.isSymmetric(root);
		System.out.println(res);
	}

	public boolean isSymmetric(TreeNode root) {
		if (root == null) {
			return true;
		}
		return check(root.left, root.right);
	}

	private boolean check(TreeNode left, TreeNode right) {
		if(left==null && right==null){
			return true;
		}
		if(left==null || right==null){
			return false;
		}
		return left.val==right.val && check(left.left,right.right) && check(left.right,right.left);
	}

}
