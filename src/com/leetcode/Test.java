package com.leetcode;

import java.util.Arrays;

/**
 * Created by rfma on 17/1/20.
 */
public class Test {
	public static void main(String[] args) {
		Test test = new Test();
		int[] a = {5,3,2,6,2,9,1};
		test.insertSort(a);
		System.out.println(Arrays.toString(a));
		String s = "abcdef";
		int m = 2;
		int n = s.length();
		char[] arr = s.toCharArray();
		reverseString(arr, 0, m - 1); //反转[0..m - 1]，套用到上面举的例子中，就是X->X^T，即 abc->cba
		reverseString(arr, m, n - 1); //反转[m..n - 1]，例如Y->Y^T，即 def->fed
		reverseString(arr, 0, n - 1); //反转[0..n - 1]，即如整个反转，(X^TY^T)^T=YX，即 cbafed->defabc。
		System.out.println(Arrays.toString(arr));
	}

	public static void reverseString(char[] s, int from, int to) {
		while (from < to) {
			char c = s[from];
			s[from++] = s[to];
			s[to--] = c;
		}
	}

	public static void insertSort(int[] value) {

		int i, j, temp;
		// 从第二个数开始，插入前面的有序序列
		for (i = 1; i <= value.length - 1; i++) {
			// 记录将要插入的记录
			temp = value[i];
			// 从该记录前面的一条记录开始比较，如果比前面的数小，那么不断后移
			for (j = i - 1; j >= 0; j--) {
				if (temp < value[j]) {
					value[j + 1] = value[j];
				} else
					break;
			}
			value[j + 1] = temp;
		}
	}

	public void quickSort(int[] a, int left, int right) {
		if (left < right) {
			int p = partition(a, left, right);
			quickSort(a, left, p - 1);
			quickSort(a, p + 1, right);
		}
	}

	private int partition(int[] a, int left, int right) {
		int value = a[left];
		while (left < right) {
			while (left < right && a[right] > value) {
				right--;
			}
			a[left] = a[right];
			while (left < right && a[left]<=value) {
				left++;
			}
			a[right] = a[left];
		}
		a[left] = value;
		return left;
	}
}
