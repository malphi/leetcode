package com.leetcode;

public class SameTree {

	/**
	 * Definition for binary tree public class TreeNode { int val; TreeNode
	 * left; TreeNode right; TreeNode(int x) { val = x; } }
	 */
	public boolean isSameTree(Node p, Node q) {
		if (p == null && q == null) {
			return true;
		}
		if (p == null | q == null) {
			return false;
		}
		if (p.val != q.val) {
			return false;
		}
		if(!isSameTree(p.left,q.left)){
			return false;
		}
		if(!isSameTree(p.right,q.right)){
			return false;
		}
		return true;

	}

	public static void main(String[] args) {
		Node p = new Node(1);
		p.left = new Node(2);
		Node q = new Node(1);
		q.left = new Node(3);
		SameTree c = new SameTree();
		System.out.println(c.isSameTree(p, q));
	}
}

class Node {
	int val;
	Node left;
	Node right;

	Node(int x) {
		val = x;
	}
}