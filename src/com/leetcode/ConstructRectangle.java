package com.leetcode;

import java.util.Arrays;

/**
 * Created by rfma on 17/1/23.
 */
public class ConstructRectangle {
	public static void main(String[] args) {
		ConstructRectangle c = new ConstructRectangle();
		int[] ret = c.constructRectangle(159);
		System.out.println(Arrays.toString(ret));
	}
	public int[] constructRectangle(int area) {
		int w = (int) Math.sqrt(area);
		while (area % w != 0) {
			w--;
		}
		return new int[]{area / w, w};
	}
}
