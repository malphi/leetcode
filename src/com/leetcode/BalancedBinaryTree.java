package com.leetcode;

/**
 * 平衡二叉树：左右子树深度不能超过1
 * 
 * @author mrf
 *
 */
public class BalancedBinaryTree {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(0);
		TreeNode l1 = new TreeNode(1);
		TreeNode l2 = new TreeNode(11);
		// TreeNode r1 = new TreeNode(2);
		root.left = l1;
		// root.right = r1;
		l1.left = l2;
		BalancedBinaryTree bbt = new BalancedBinaryTree();
		System.out.println(bbt.isBalanced(root));
	}

	public int depth(TreeNode root) {
		if (root == null)
			return 0;
		return 1 + Math.max(depth(root.left), depth(root.right));
	}

	public boolean isBalanced(TreeNode root) {
		if (root == null)
			return true;
		int leftDepth = depth(root.left);
		int rightDepth = depth(root.right);
		if (Math.abs(leftDepth - rightDepth) > 1)
			return false;
		else
			return isBalanced(root.left) && isBalanced(root.right);
	}

}
