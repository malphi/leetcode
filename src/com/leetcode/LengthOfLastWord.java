package com.leetcode;

public class LengthOfLastWord {

	public static void main(String[] args) {
		String s = "Hello world";
		LengthOfLastWord bean = new LengthOfLastWord();
		System.out.println(bean.lengthOfLastWord3(s));
	}

	public int lengthOfLastWord(String s) {
		s = s.trim();
		if (s == null || s.length() == 0) {
			return 0;
		}
		String[] arr = s.split(" ");
		return arr[arr.length - 1].length();
	}

	public int lengthOfLastWord2(String s) {
		s = s.trim();
		if (s == null || s.length() == 0) {
			return 0;
		}
		int len = 0;
		for (int i = 0; i < s.length(); i++) {
			len++;
			if (String.valueOf(s.charAt(i)).equals(" ")) {
				len = 0;
			}
		}
		return len;
	}

	public int lengthOfLastWord3(String s) {
		s = s.trim();
		if (s == null || s.length() == 0) {
			return 0;
		}
		int start = s.lastIndexOf(" ");
		return s.length() - 1 - start;
	}
}
