package com.leetcode;


public class ReverseInteger {
	public static void main(String[] args) {
		ReverseInteger r = new ReverseInteger();
		System.out.println(r.reverse(-123));
	}
	
	public int reverse(int x) {
		if(x==0){
			return x;
		}else if(x>0){
			StringBuilder sb = new StringBuilder(String.valueOf(x));
			x = Integer.parseInt(sb.reverse().toString());
		}else{
			StringBuilder sb = new StringBuilder(String.valueOf(-x));
			x = -Integer.parseInt(sb.reverse().toString());
		}
        return x;
    }
}
