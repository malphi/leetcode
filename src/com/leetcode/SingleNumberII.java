package com.leetcode;

import java.util.HashMap;
import java.util.Map;

public class SingleNumberII {

	public static int singleNumber(int[] A) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < A.length; i++) {
			int x = A[i];
			if (map.get(x) == null) {
				map.put(x, 1);
			} else {
				map.put(x, map.get(x) + 1);
			}
		}
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 1) {
				return entry.getKey();
			}
		}
		return 0;

	}

	public static int singleNumber2(int[] A) {
		int ones = 0, twos = 0, xthrees = 0;
		for (int i = 0; i < A.length; ++i) {
			twos |= (ones & A[i]);
			ones ^= A[i];
			xthrees = ~(ones & twos);
			ones &= xthrees;
			twos &= xthrees;
		}

		return ones;
	}

	public static void main(String[] args) {
		int[] A = { 2, 2, 2, 3 };
		int x = singleNumber2(A);
		System.out.println(x);
	}

}
