package com.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class BinaryTreeLevelOrderTraversalII {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(3);
		TreeNode left = new TreeNode(9);
		TreeNode right = new TreeNode(20);
		root.left = left;
		root.right = right;
		right.left = new TreeNode(15);
		right.right = new TreeNode(7);
		List<List<Integer>> list = levelOrderBottom(root);
		System.out.println(list);
	}

	public static List<List<Integer>> levelOrderBottom(TreeNode root) {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		Stack<List<Integer>> stack = new Stack<>();
		order(stack, 0, root);

		Iterator<List<Integer>> it = stack.iterator();
		while (it.hasNext()) {
			list.add(stack.pop());
		}
		return list;
	}

	private static void order(Stack<List<Integer>> list, int level,
			TreeNode root) {
		if (root == null) {
			return;
		}

		if (list.size() <= level) {
			List<Integer> levellist = new ArrayList<Integer>();
			list.push(levellist);
		}
		List<Integer> levellist = list.get(level);
		if (levellist == null) {
			levellist = new ArrayList<Integer>();
			list.push(levellist);
		}
		levellist.add(root.val);
		order(list, level + 1, root.left);
		order(list, level + 1, root.right);

	}
}
