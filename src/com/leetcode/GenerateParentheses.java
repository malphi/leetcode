package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class GenerateParentheses {

	public static void main(String[] args) {
		GenerateParentheses gp = new GenerateParentheses();
		List<String> list =gp.generateParenthesis(3);
		System.out.println(list);
	}

	public List<String> generateParenthesis(int n) {
		List<String> list = new ArrayList<>();
		generate(n, n, "", list);
		return list;
	}

	private void generate(int leftNum, int rightNum, String s,
			List<String> result) {
		if (leftNum == 0 && rightNum == 0) {
			result.add(s);
		}
		if (leftNum > 0) {
			generate(leftNum - 1, rightNum, s + '(', result);
		}
		if (rightNum > 0 && leftNum < rightNum) {
			generate(leftNum, rightNum - 1, s + ')', result);
		}
	}
}
