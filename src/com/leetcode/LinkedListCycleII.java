package com.leetcode;

public class LinkedListCycleII {

	public static void main(String[] args) {

	}

	/**
	 * 也就是从head到环开始的路程 ＝ 从相遇到环开始的路程 那么。。。只要S和F相遇了，我们拿一个从头开始走，一个从相遇的地方开始走
	 * 两个都走一步，那么再次相遇必定是环的开始节点！
	 * 
	 * @param head
	 * @return
	 */
	public ListNode detectCycle(ListNode head) {
		if (head == null) {
			return null;
		}
		ListNode fast = head;
		ListNode slow = head;
		while (fast != null && fast.next != null) {
			fast = fast.next.next;
			if (slow != null)
				slow = slow.next;
			if (fast != null && fast == slow) {
				slow = head;
				while (slow != fast) {
					slow = slow.next;
					fast = fast.next;
				}
				return slow;
			}
		}
		return null;

	}
}
