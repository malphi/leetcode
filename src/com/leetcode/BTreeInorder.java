package com.leetcode;

import java.util.ArrayList;
import java.util.Stack;

public class BTreeInorder {
	private static ArrayList<Integer> list = new ArrayList<Integer>();

	public ArrayList<Integer> inorderTraversal1(TreeNode root) {
		if (root == null) {
			return list;
		}
		inorderTraversal1(root.left);
		list.add(root.val);
		inorderTraversal1(root.right);
		return list;
	}
	
	public ArrayList<Integer> inorderTraversal2(TreeNode root) {
		if (root == null) {
			return list;
		}
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode node = root;
		while(!stack.isEmpty() || node!=null){
			if(node!=null){
				stack.push(node);
				node = node.left;
			}else{
				node = stack.pop();
				list.add(node.val);
				node = node.right;
			}
		}
		return list;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		// TreeNode right1 = new TreeNode(0);
		// root.right = right1;
		TreeNode left2 = new TreeNode(2);
		root.left = left2;
		System.out.println("1=" + BTreeInorder.list);
		BTreeInorder bean = new BTreeInorder();
		bean.inorderTraversal2(root);
		System.out.println("2=" + BTreeInorder.list);
	}
}

