package com.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortLisst {

	public ListNode sortList(ListNode head) {
		if (head == null || head.next == null)
			return head;
		List<ListNode> list = new ArrayList<ListNode>();
		ListNode node = head;
		do {
			list.add(node);
			node = node.next;
		} while (node != null);
		Collections.sort(list, new Com());
		for (int i = 0; i < list.size(); i++) {
			ListNode n = list.get(i);
			n.next = null;
			if (i < list.size() - 1) {
				n.next = list.get(i + 1);
			}
		}
		head = list.get(0);
		return head;
	}

	public static void main(String[] args) {
		ListNode tail = new ListNode(5);
		ListNode mid = new ListNode(3);
		ListNode head = new ListNode(7);
		head.next = mid;
		mid.next = tail;
		System.out.println(head);
		SortLisst s = new SortLisst();
		s.sortList(head);
		System.out.println(head);
	}
}

class Com implements Comparator<ListNode> {

	@Override
	public int compare(ListNode n1, ListNode n2) {
		if (n1.val > n2.val) {
			return 1;
		}
		if (n1.val < n2.val) {
			return -1;
		}
		return 0;
	}

}

//class ListNode {
//	int val;
//	ListNode next;
//
//	ListNode(int x) {
//		val = x;
//		this.next = null;
//	}
//
//}
