package com.leetcode;

public class LinkedListCycle {

	public static void main(String[] args) {
		LN head = new LN(1);
		LN tail = new LN(2);
		head.next = tail;
		tail.next = head;
		LinkedListCycle c = new LinkedListCycle();
		System.out.println(c.hasCycle(head));
	}

	public boolean hasCycle(LN head) {
		if(head==null){
			return false;
		}
		LN fast = head;
		LN slow = head;
		while(fast!=null && fast.next!=null){
			slow = slow.next;
			fast = fast.next.next;
			if(slow==fast){
				return true;
			}
		}
		return false;
	}
}

class LN {
	int val;
	LN next;

	LN(int x) {
		val = x;
		next = null;
	}
}