package com.leetcode;

import java.math.BigDecimal;

public class MultiString {
	public static void main(String[] args) {
		String s = multiply("2","4");
		System.out.println(s);
	}
	
	public static String multiply(String num1, String num2) {
		
		BigDecimal b1 = new BigDecimal(num1);
		BigDecimal b2 = new BigDecimal(num2);
		BigDecimal res = b1.multiply(b2);
		return res.toString();
    }
}
