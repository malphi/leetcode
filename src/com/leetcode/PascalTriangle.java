package com.leetcode;

import java.util.ArrayList;

public class PascalTriangle {
	// a[ i ][ j ] = a[i - 1] [j - 1] + a[i - 1][ j ]
	public static ArrayList<ArrayList<Integer>> generate(int numRows) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<>();
		for (int i = 0; i < numRows; i++) {
			ArrayList<Integer> l = new ArrayList<Integer>(i);
			list.add(l);
			if (i == 0) {
				l.add(1);
				continue;
			}
			for (int j = 0; j <= i; j++) {
				ArrayList<Integer> tmp = list.get(i - 1);
				int x = (j - 1 < 0) ? 0 : tmp.get(j - 1);
				int y = j >= tmp.size() ? 0 : tmp.get(j);
				l.add(x+y);
			}
		}
		return list;

	}

	public static void main(String[] args) {
		System.out.println(generate(10));
	}
}
