package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Given a collection of numbers, return all possible permutations. For example,
 * [1,2,3] have the following permutations: [1,2,3], [1,3,2], [2,1,3], [2,3,1],
 * [3,1,2], and [3,2,1].
 * 
 * @author mrf
 *
 */
public class Permutations {

	public static void main(String[] args) {
		int[] a = { 1, 2, 3 };
		Permutations p = new Permutations();
		System.out.println(p.permute(a));
	}

	public List<List<Integer>> permute(int[] num) {
		List<List<Integer>> result = new ArrayList<>();
		if (num == null || num.length < 1)
			return result;
		dfs(0, num, result);
		return result;
	}

	private static void dfs(int i, int[] num, List<List<Integer>> result) {
		int n = num.length;
		if (i == n) {
			List<Integer> l = new ArrayList<>();
			for (int j = 0; j < num.length; j++) {
				l.add(num[j]);
			}
			result.add(l);
			return;
		}
		for (int j = i; j < n; j++) {
			swap(num, i, j);
			dfs(i + 1, num, result);
			swap(num, j, i);
		}
		return;
	}

	private static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}
