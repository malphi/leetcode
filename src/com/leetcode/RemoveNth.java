package com.leetcode;

public class RemoveNth {

	public ListNode removeNthFromEnd(ListNode head, int n) {
		ListNode p1 = head;
		ListNode p2 = head;
		ListNode removeNode = head;
		int i = 0;
		while (p1 != null) {
			p1 = p1.next;
			i++;
			if (i > n) {
				p2 = removeNode;
				removeNode = removeNode.next;
			}
		}

		if (removeNode == head) {
			head = head.next;
		} else {
			p2.next = removeNode.next;
		}
		return head;
	}

	public static void main(String[] args) {
		RemoveNth rn = new RemoveNth();
		ListNode head = new ListNode(4);
		ListNode two = new ListNode(5);
		ListNode three = new ListNode(4);
		head.next = two;
		two.next = three;
		rn.removeNthFromEnd(head, 3);
		System.out.println(head.next.val);
	}
}
