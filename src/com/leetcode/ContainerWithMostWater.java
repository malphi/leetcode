package com.leetcode;

public class ContainerWithMostWater {

	public static void main(String[] args) {

	}

	public int maxArea(int[] height) {
		if (height == null || height.length == 0) {
			return 0;
		}
		int c = 0;
		int left = 0;
		int right = height.length - 1;
		while (left < right) {
			int temp = Math.min(height[left], height[right]) * (right - left);
			if (temp > c) {
				c = temp;
			}
			if (height[left] < height[right]) {
				left++;
			} else {
				right--;
			}
		}
		return c;
	}
}
