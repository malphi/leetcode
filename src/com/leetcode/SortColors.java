package com.leetcode;

public class SortColors {

	public void sortColors(int[] A) {
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A.length; j++) {
				if (j > i && A[j] < A[i]) {
					int tmp = A[j];
					A[j] = A[i];
					A[i] = tmp;
				}
			}
		}
	}

	public static void main(String[] args) {
		int[] A = { 1, 2, 0, 0, 2, 1, 2 };
		SortColors s = new SortColors();
		 s.sortColors(A);
//		s.quickSort(A);
		for (int i = 0; i < A.length; i++) {
			System.out.print(A[i] + " ");
		}
	}
}
