package com.leetcode;

public class BestTimetoBuyandSellStock {

	public static void main(String[] args) {
		BestTimetoBuyandSellStock b = new BestTimetoBuyandSellStock();
		int[] prices = { 5, 2, 11, 14, 2 };
		int x = b.maxProfit(prices);
		System.out.println("x=" + x);
	}

	public int maxProfit(int[] prices) {
		if (prices.length <= 1) {
			return 0;
		}
		int max = 0;
		int min = prices[0];
		for (int i = 1; i < prices.length; i++) {
			if (prices[i] > min) {
				max = (prices[i] - min) > max ? (prices[i] - min) : max;
			} else {
				min = prices[i];
			}
		}
		return max;
	}

	public int maxProfit2(int[] prices) {
		int max = 0;
		for (int i = 0; i < prices.length - 1; i++) {
			for (int j = i + 1; j < prices.length; j++) {
				int temp = prices[j] - prices[i];
				if (temp > max) {
					max = temp;
				}
			}
		}
		return max;
	}
}
