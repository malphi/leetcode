package com.leetcode;

public class PathSum {

	public static void main(String[] args) {
		
	}

	public boolean hasPathSum(TreeNode root, int sum) {
		if (root == null) {
			return false;
		}
		sum -= root.val;
		if (root.left == null && root.right == null) {
			return sum == 0;
		}

		return hasPathSum(root.left, sum) || hasPathSum(root.right, sum);
	}

//	private boolean dfs(TreeNode node, int sum) {
//
//		if (node == null) {
//			return false;
//		}
//		sum -= node.val;
//		if (node.left == null && node.right == null) {
//			return sum == 0;
//		}
//
//		return dfs(node.left, sum) || dfs(node.right, sum);
//	}

}
