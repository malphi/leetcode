package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreeLevelOrderTraversal {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(3);
		TreeNode left = new TreeNode(9);
		TreeNode right = new TreeNode(20);
		root.left = left ;
		root.right = right;
		right.left = new TreeNode(15);
		right.right = new TreeNode(7);
		List<List<Integer>> list = levelOrder(root);
		System.out.println(list);
	}

	public static List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> list = new ArrayList<List<Integer>>(10);
		
		order(list,0,root);
		return list;
	}

	private static void order(List<List<Integer>> list, int level, TreeNode root) {
		if(root==null){
			return;
		}
		if(list.size()<=level){
			List<Integer> levellist = new ArrayList<Integer>();
			list.add(levellist);
		}
		List<Integer> levellist = list.get(level);
		if(levellist==null){
			levellist = new ArrayList<Integer>();
			list.add(levellist);
		}
		levellist.add(root.val);
		order(list,level+1,root.left);
		order(list,level+1,root.right);
		
	}
}
