package com.leetcode;

public class GasStation {

	public int canCompleteCircuit2(int[] gas, int[] cost) {
		int size = cost.length;
		for (int i = 0; i < size; i++) {
			int sc = 0;
			int sg = 0;
			boolean all = true;
			for (int j = 0; j < size; j++) {
				int x = j - i >= 0 ? j - i : i - j;
				if (x > size - 1) {
					x = x - size;
				}
				sg += gas[x];
				sc += cost[x];
				if (sg - sc < 0) {
					all = false;
					break;
				}
			}
			if (all) {
				return i;
			}
		}
		return -1;
	}

	public int canCompleteCircuit(int[] gas, int[] cost) {
		int sum = 0;
		int total = 0;
		int j = -1;
		for (int i = 0; i < gas.length; ++i) {
			sum += gas[i] - cost[i];
			total += gas[i] - cost[i];
			if (sum < 0) {
				j = i;
				sum = 0;
			}
		}
		if (total < 0)
			return -1;
		else
			return j + 1;
	}

	public static void main(String[] args) {
		int[] cost = { 2, 1, 5, 1 };
		int[] gas = { 1, 2, 3, 3 };
		GasStation gs = new GasStation();
		int i = gs.canCompleteCircuit(gas, cost);
		System.out.println(i);
	}
}
