package com.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BinaryTreePostorderTraversal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public List<Integer> postorderTraversal(TreeNode root) {
		List<Integer> list = new ArrayList<>();
		travel(list, root);
		return list;

	}

	private void travel(List<Integer> list, TreeNode node) {
		if (node == null) {
			return;
		}
		travel(list, node.right);
		list.add(node.val);
		travel(list, node.left);
	}

	/**
	 * <pre>
	 * 后序遍历结点的访问顺序是：左儿子 -> 右儿子 -> 自己。那么一个结点需要两种情况下才能够输出：
	 * 第一，它已经是叶子结点；第二，它不是叶子结点，但是它的儿子已经输出过。那么基于此我们只需要记录一下当前输出的结点即可。
	 * 对于一个新的结点，如果它不是叶子结点，儿子也没有访问，那么就需要将它的右儿子，左儿子压入。
	 * 如果它满足输出条件，则输出它，并记录下当前输出结点。输出在stack为空时结束
	 * </pre>
	 * 
	 * @param root
	 * @return
	 */
	public List<Integer> postorderTraversal2(TreeNode root) {
		List<Integer> result = new ArrayList<>();
		Stack<TreeNode> stack = new Stack<>();
		if (root == null)
			return result;
		stack.push(root);
		TreeNode head = root;
		while (!stack.empty()) {
			TreeNode cur = stack.peek();
			if (cur.right == head || cur.left == head
					|| ((cur.right == null) && (cur.left == null))) {
				stack.pop();
				result.add(cur.val);
				head = cur;
			} else {
				if (cur.right != null)
					stack.push(cur.right);
				if (cur.left != null)
					stack.push(cur.left);
			}
		}
		return result;

	}
}
