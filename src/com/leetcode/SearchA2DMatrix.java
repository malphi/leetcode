package com.leetcode;

public class SearchA2DMatrix {

	public static void main(String[] args) {
		int[][] m = { { 1, 3, 5, 7 }, { 10, 11, 16, 20 }, { 23, 30, 34, 50 } };
		SearchA2DMatrix sm = new SearchA2DMatrix();
		System.out.println(sm.searchMatrix3(m, 3));
	}

	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0) {
			return false;
		}
		for (int i = 0; i < matrix.length; i++) {
			int[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				int x = row[j];
				if (x == target) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean searchMatrix2(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0) {
			return false;
		}
		for (int i = matrix.length - 1; i >= 0; i--) {
			int[] row = matrix[i];
			if (row[0] > target) {
				continue;
			}
			for (int j = 0; j < row.length; j++) {
				int x = row[j];
				if (x == target) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean searchMatrix3(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
			return false;

		int m = matrix.length;
		int n = matrix[0].length;

		int start = 0;
		int end = m * n - 1;

		while (start <= end) {
			int mid = (start + end) / 2;
			int midX = mid / n;
			int midY = mid % n;

			if (matrix[midX][midY] == target)
				return true;

			if (matrix[midX][midY] < target) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}

		return false;
	}
}
