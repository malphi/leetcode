package com.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ValidSudoku {

	public static void main(String[] args) {
		ValidSudoku vs = new ValidSudoku();
		char[][] board = { { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
				{ '6', '8', '.', '1', '9', '5', '.', '.', '.' },
				{ '.', '9', '.', '.', '.', '.', '.', '6', '.' },
				{ '8', '.', '.', '.', '1', '.', '.', '.', '.' },
				{ '4', '.', '.', '.', '2', '.', '.', '.', '.' },
				{ '7', '.', '.', '.', '3', '.', '.', '.', '.' },
				{ '.', '6', '.', '.', '4', '.', '.', '.', '.' },
				{ '.', '.', '.', '.', '5', '.', '.', '.', '.' },
				{ '.', '.', '.', '.', '6', '.', '.', '.', '.' } };
		System.out.println(vs.isValidSudoku(board));
	}

	public boolean isValidSudoku(char[][] board) {
		Set<Character> rows = new HashSet<>();
		Set<Character> cols = new HashSet<>();

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i][j] != '.') {
					if (rows.contains(board[i][j])) {
						return false;
					}
					rows.add(board[i][j]);
				}
				if (board[j][i] != '.') {
					if (cols.contains(board[j][i]))
						return false;
					cols.add(board[j][i]);
				}
			}
			rows.clear();
			cols.clear();
		}
		// check grid key = i/3_j/3
		Map<String, Set<Character>> map = new HashMap<>();
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				String key = buildKey(i, j);
				if (map.get(key) == null)
					map.put(key, new HashSet<Character>());
			}
		}
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				char c = board[j][i];
				if (c == '.') {
					continue;
				}
				String key = buildKey(i, j);
				Set<Character> temp = map.get(key);
				if (temp.contains(c)) {
					return false;
				}
				temp.add(c);
			}
		}
		return true;
	}

	private String buildKey(int i, int j) {
		return String.valueOf(i / 3) + "_" + String.valueOf(j / 3);
	}
}
