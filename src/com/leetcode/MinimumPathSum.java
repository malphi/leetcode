package com.leetcode;

public class MinimumPathSum {

	static int[][] grid = { { 1, 2, 3 }, { 3, 4, 2 }, { 1, 1, 1 } };
	static int[][] res = null;

	public static void main(String[] args) {
		show(grid);
		int x = minPathSum(grid);
		System.out.println("--------------" + x);
		show(res);
	}

	private static void show(int[][] grid) {
		int m = grid.length;
		int n = grid[0].length;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(grid[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static int minPathSum(int[][] grid) {
		int m = grid.length;
		int n = grid[0].length;
		res = new int[m][n];
		res[0][0] = grid[0][0];
		for (int i = 1; i < m; i++) {
			res[i][0] = grid[i][0] + res[i - 1][0];
		}
		for (int i = 1; i < n; i++) {
			res[0][i] = grid[0][i] + res[0][i - 1];
		}
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				res[i][j] = grid[i][j] + Math.min(res[i - 1][j], res[i][j - 1]);
			}
		}
		return res[m - 1][n - 1];
	}

}
