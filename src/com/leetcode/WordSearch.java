package com.leetcode;

public class WordSearch {


	public static void main(String[] args) {
		char[][] array = { { 'a', 'b', 'c', 'e' }, { 's', 'f', 'c', 's' }, { 'a', 'd', 'e', 'e' } };
		String word = "abcb";
		System.out.println(exist(array, word));
	}

	private static char[] wordCharArray;
	private static int L;
	private static char[][] board;
	private static int M;
	private static int N;
	private static boolean[][] mark;
	public static boolean exist(char[][] board, String word) {
		if (word == null || word.length() == 0 || board == null || board.length == 0) {
			return false;
		}
		wordCharArray = word.toCharArray();
		L = wordCharArray.length;
		WordSearch.board = board;
		M = board.length;
		N = board[0].length;
		mark = new boolean[M][N];
		for (int i = 0; i < M; ++i) {
			for (int j = 0; j < N; ++j) {
				if (dfs(i, j, 0)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean dfs(int i, int j, int k) {
		if (i < 0 || j < 0 || i >= M || j >= N || mark[i][j]) {
			return false;
		}
		if (board[i][j] != wordCharArray[k]) {
			return false;
		}
		if (k == L - 1) {
			return true;
		}
		mark[i][j] = true;
		boolean result = dfs(i - 1, j, k + 1) || dfs(i + 1, j, k + 1) || dfs(i, j - 1, k + 1) || dfs(i, j + 1, k + 1);
		mark[i][j] = false;
		return result;
	}

}
