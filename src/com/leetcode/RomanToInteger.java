package com.leetcode;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {
	public static final Map<String, Integer> map = new HashMap<>();
	static {
		map.put("I", 1);
		map.put("V", 5);
		map.put("X", 10);
		map.put("L", 50);
		map.put("C", 100);
		map.put("D", 500);
		map.put("M", 1000);
	}

	public static void main(String[] args) {
		String s = "MMCMXCIX";
		int x = romanToInt(s);
		System.out.println(x);
	}

	// 相同的数字连写，所表示的数等于这些数字相加得到的数，如：Ⅲ = 3；
	// 小的数字在大的数字的右边，所表示的数等于这些数字相加得到的数， 如：Ⅷ = 8；Ⅻ = 12；
	// 小的数字，（限于Ⅰ、X 和C）在大的数字的左边，所表示的数等于大数减小数得到的数，如：Ⅳ= 4；Ⅸ= 9；
	// 正常使用时，连写的数字重复不得超过三次。（表盘上的四点钟“IIII”例外）
	// MMMCMXCIX,3999
	public static int romanToInt(String s) {
		if (s == null || s.length() == 0)
			return 0;
		int res = 0;
		int last = 0;
		for (int i = 0; i < s.length(); i++) {
			String c = String.valueOf(s.charAt(i));
			int value = map.get(c);
			if (last == 0) {
				last = value;
				res = value;
			} else {
				if (value == last) {
					res += value;
				} else if (value < last) {
					res += value;
				} else {
					res = res + value - 2 * last;
				}
				last = value;
			}
		}
		return res;
	}
}
