package com.leetcode;

import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicateSortedArray {

	public static void main(String[] args) {
		int[] A = { -1, 0, 0, 0, 0, 3, 3 };
		int x = removeDuplicates2(A);
		System.out.println(x);
	}

	public static int removeDuplicates(int[] A) {
		if (A == null || A.length < 2) {
			return A == null ? 0 : A.length;
		}
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < A.length; i++) {
			set.add(A[i]);
		}
		int i = 0;
		for (Integer x : set) {
			A[i] = x;
			i++;
		}
		return set.size();

	}

	public static int removeDuplicates2(int[] A) {
		if (A == null || A.length == 0)
			return 0;
		int startPosition = 0;
		for (int i = 1; i < A.length; i++) {
			if (A[i] != A[startPosition]) {
				A[startPosition + 1] = A[i];
				startPosition++;
			}
		}
		return startPosition + 1;

	}
}
