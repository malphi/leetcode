package com.leetcode;

/**
 * Created by malphi on 17/1/24.
 * formula: https://en.wikipedia.org/wiki/Digital_root#Congruence_formula
 * {\mbox{dr}}(n)=1\ +\ ((n-1)\ {{\rm {mod}}}\ 9).\
 */
public class AddDigit {
    public static void main(String[] args) {
        System.out.println(addDigits2(38));
    }

    public static int addDigits(int num) {
        if (num < 10) {
            return num;
        }
        String str = String.valueOf(num);
        int ret = 0;
        for (int i = 0; i < str.length(); i++) {
            int x = Integer.parseInt(str.charAt(i) + "");
            ret += x;
        }
        return addDigits(ret);
    }

    public int addDigits3(int num) {
        return 1 + (num - 1) % 9;
    }
    public static int addDigits2(int num) {
        return num == 0 ? 0 : (num % 9 == 0 ? 9 : (num % 9));
    }
}
